package com.dino

import com.badlogic.gdx.ApplicationAdapter
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Input
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.utils.ScreenUtils
import com.dino.dinos.*
import com.dino.world.Tile

class DinoGame : ApplicationAdapter() {
    lateinit var batch: SpriteBatch
    lateinit var background: Texture
    lateinit var camera: OrthographicCamera
    lateinit var dilop: Dilop
    lateinit var raptor: RedRaptor
    lateinit var dime: Dimetrodon
    lateinit var purpPt: PurplePtera
    val tileList = ArrayList<Tile>()


    var dinoPosition = Vector2(10f, 10f)
    var dinoVelocity = Vector2(0f, 0f)
    var forwardVelocity = Vector2(10f, 0f)
    var backwardVelocity = Vector2(-10f, 0f)

    var jumpVelocity = 80f
    var gravity = Vector2(0f, -20f)
    lateinit var rect1:Vector2


    lateinit var currentDino : Dino

    override fun create() {
        batch = SpriteBatch()
        camera = OrthographicCamera()
        background = Texture("world/png/BG.png")
        dilop = Dilop()
        raptor = RedRaptor()
        dime = Dimetrodon()
        purpPt = PurplePtera()
        camera.setToOrtho(false, 1920f, 1080f)
        generateLevel()
    }

    override fun render() {
        dilop.state += Gdx.graphics.deltaTime
        raptor.state += Gdx.graphics.deltaTime
        dime.state += Gdx.graphics.deltaTime
        purpPt.state += Gdx.graphics.deltaTime
        camera.update()
        ScreenUtils.clear(1f, 0f, 0f, 1f)
        batch.projectionMatrix = camera.combined
        batch.begin()
        batch.draw(background, 0f, 0f)
        tileList.forEach{ batch.draw(Texture(it.fileName), it.x.toFloat(), it.y.toFloat())}
        batch.draw(dilop.currentAnimation.getKeyFrame(dilop.state), dinoPosition.x, dinoPosition.y)
        //batch.draw(raptor.walk.getKeyFrame(raptor.state), 1000f, 500f)
        //batch.draw(dime.walk.getKeyFrame(dime.state), 10f, 500f)
        //batch.draw(purpPt.currentAnimation.getKeyFrame(purpPt.state), purpPt.xPos, purpPt.yPos)

        if (Gdx.input.isKeyPressed(Input.Keys.RIGHT)){
            if (Gdx.input.isKeyPressed(Input.Keys.SHIFT_RIGHT) || Gdx.input.isKeyPressed(Input.Keys.SHIFT_LEFT)){
                dilop.currentAnimation = dilop.run
                //dinoVelocity.add(forwardVelocity)
                dinoPosition.add(forwardVelocity)
            } else {
                dilop.currentAnimation = dilop.walk
                //dinoVelocity.add(forwardVelocity)
                dinoPosition.add(forwardVelocity)
            }
        } else if (Gdx.input.isKeyPressed(Input.Keys.LEFT)) {
            if (Gdx.input.isKeyPressed(Input.Keys.SHIFT_RIGHT) || Gdx.input.isKeyPressed(Input.Keys.SHIFT_LEFT)) {
                dilop.currentAnimation = dilop.run
                // dinoVelocity.add(backwardVelocity)
                dinoPosition.add(backwardVelocity)
            } else {
                dilop.currentAnimation = dilop.walk
                //dinoVelocity.add(backwardVelocity)
                dinoPosition.add(backwardVelocity)
            }
        } else if(!Gdx.input.isKeyPressed(Input.Keys.RIGHT) || !Gdx.input.isKeyPressed(Input.Keys.LEFT)){
            dilop.currentAnimation = dilop.idle

//            if (!Gdx.input.isKeyPressed(Input.Keys.RIGHT) && dinoVelocity.x > 0) {
//                dinoVelocity.add(-10f, 0f)
//            } else if (!Gdx.input.isKeyPressed(Input.Keys.LEFT) && dinoVelocity.x < 0) {
//                dinoVelocity.add(10f, 0f)
//            }
        }

        if(Gdx.input.isKeyPressed(Input.Keys.SPACE)){
            dinoVelocity.add(0f, jumpVelocity)
            dilop.currentAnimation = dilop.walk

        }

        dinoVelocity.add(gravity)

        dinoPosition.mulAdd(dinoVelocity, Gdx.graphics.deltaTime)
        if (dinoPosition.y < 5f) {
            dinoPosition.y = 5f
            dinoVelocity.y = gravity.y
        }
        //batch!!.draw(img, 0f, 0f)
        batch.end()
    }

    override fun dispose() {
        batch.dispose()
    }

    private fun generateLevel() {
//        val file = File("/Users/brandonlanthrip/Desktop/level.json")
//        val json = JSONArray(file.readText())
//
//        json.forEach {
//            val obj = it as JSONObject
//            tileList.add(Tile(obj.getString("tile"), obj.getInt("x"), obj.getInt("y")))
//        }
    }
}